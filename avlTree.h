//
// Created by kharivitalij on 28.4.18.
//

#ifndef AVLTREE_AVLTREE_H
#define AVLTREE_AVLTREE_H

#include "node.h"
#include "pair.h"

class avlTree {
public:
    Node* m_root;
    Node* add(Node* root, Node* prev, Node* item);
    Node* add(Node* item);
    Node* find(Node* root, Node* item);
    Node* rotateleft(Node* item);
    Node* rotateright(Node* item);
    Node* balance(Node* node);
    Pair* remove_min(Node* node);
    Node* remove(Node* root, Node* item);
    Node* priemnik(Node* root, Node* item);
    Node* predshestvennik(Node* root, Node* item);
    int height(Node* item);
    void show(Node* item, int tree_height, int current_height);
    void show();
    avlTree();
};

#endif //AVLTREE_AVLTREE_H
