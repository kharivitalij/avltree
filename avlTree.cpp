//
// Created by kharivitalij on 28.4.18.
//

#include "avlTree.h"

Node* avlTree::add(Node* root, Node* prev, Node* item) {
    if (root == nullptr) {
        item->m_parent = prev;
        return item;
    }
    if (item->m_key >= root->m_key) {
        root->m_right = this->add(root->m_right, root, item);
    }
    else {
        root->m_left = this->add(root->m_left, root, item);
    }
    root->fix_height();
    Node* balanced = balance(root);
    balanced->m_parent = prev;
    return balanced;
}

Node* avlTree::add(Node *item) {
    return this->add(this->m_root, nullptr, item);
}

Node* avlTree::rotateleft(Node* node) {
    Node* temp = node->m_right;
    temp->m_parent = node->m_parent;
    node->m_parent = temp;
    if (temp->m_left) temp->m_left->m_parent = node;
    node->m_right = node->m_right->m_left;
    temp->m_left = node;
    node->fix_height();
    temp->fix_height();
    return temp;
}

Node* avlTree::rotateright(Node* node) {
    Node* temp = node->m_left;
    temp->m_parent = node->m_parent;
    node->m_parent = temp;
    if (temp->m_right) temp->m_right->m_parent = node;
    node->m_left = node->m_left->m_right;
    temp->m_right = node;
    node->fix_height();
    temp->fix_height();
    return temp;
}

Node* avlTree::balance(Node* node) {
    if (abs(node->lr_diff()) < 2) return node;
    if (node->lr_diff() == -2) {
        if (node->m_right->lr_diff() <= 0) {
            return rotateleft(node);
        }
        node->m_right = rotateright(node->m_right);
        return rotateleft(node);
    }
    if (node->lr_diff() == 2) {
        if (node->m_left->lr_diff() >= 0) {
            return rotateright(node);
        }
        node->m_left = rotateleft(node->m_left);
        return rotateright(node);
    }
}

Pair* avlTree::remove_min(Node *node) {
    if (!node->m_left) {
        if (node->m_right) node->m_right->m_parent = node->m_parent;
        return new Pair(node, node->m_right);
    }
    Pair* temp = remove_min(node->m_left);
    temp->ptr->m_parent = node;
    node->m_left = temp->ptr;
    node->fix_height();
    return new Pair(temp->min, balance(node));
}

Node* avlTree::remove(Node* root, Node* item) {
    if (root) {
        if (root->m_key > item->m_key) {
            root->m_left = remove(root->m_left, item);
        }
        if (root->m_key < item->m_key) {
            root->m_right = remove(root->m_right, item);
        }
        if (root->m_key == item->m_key) {
            if (!root->m_right) return root->m_left;
            Pair* temp = remove_min(root->m_right);
            if (temp->ptr) temp->ptr->m_parent = root;
            root->m_key = temp->min->m_key;
            root->m_right = temp->ptr;
        }
        root->fix_height();
        return balance(root);
    }
    else cout<<"There is no element"<<endl;
    return root;
}

Node* avlTree::find(Node *root, Node *item) {
    if (root) {
        if (root->m_key > item->m_key) {
            return find(root->m_left, item);
        }
        if (root->m_key < item->m_key) {
            return find(root->m_right, item);
        }
        if (root->m_key == item->m_key) {
            return root;
        }
    }
    else {
        cout<<"There is no element"<<endl;
        return nullptr;
    }
}

Node* avlTree::priemnik(Node *root, Node *item) {
    Node* node = find(root, item);
    if (node->m_right) {
        Node* temp = node->m_right;
        while (temp->m_left) {
            temp = temp->m_left;
        }
        return temp;
    }
    else {
        Node* temp = node;
        while (temp->m_parent) {
            if (temp->m_parent->m_left == temp) return temp->m_parent;
            temp = temp->m_parent;
        }
        cout<<"max elem"<<endl;
        return nullptr;
    }
}

Node* avlTree::predshestvennik(Node *root, Node *item) {
    Node* node = find(root, item);
    if (node->m_left) {
        Node* temp = node->m_left;
        while (temp->m_right) {
            temp = temp->m_right;
        }
        return temp;
    }
    else {
        Node* temp = node;
        while (temp->m_parent) {
            if (temp->m_parent->m_right == temp) return temp->m_parent;
            temp = temp->m_parent;
        }
        cout<<"min elem"<<endl;
        return nullptr;
    }
}

int avlTree::height(Node *item) {
    if (item == nullptr) {
        cout<<"Tree is empty"<<endl;
        return 0;
    }
    if (item->m_left == nullptr && item->m_right == nullptr) return 0;
    if (item->m_left != nullptr && item->m_right == nullptr) return this->height(item->m_left) + 1;
    if (item->m_left == nullptr && item->m_right != nullptr) return this->height(item->m_right) + 1;
    return max(this->height(item->m_left), this->height(item->m_right)) + 1;
}

void avlTree::show(Node* item, int tree_height, int current_height) {
    if (item) {
        if (item->m_left) show(item->m_left, tree_height, current_height + 1);
        item->show(tree_height, current_height);
        if (item->m_right) show(item->m_right, tree_height, current_height + 1);
    }
    else {
        cout<<"Tree is empty"<<endl;
        return;
    }
}

void avlTree::show() {
    this->show(this->m_root, this->height(this->m_root), 0);
}

avlTree::avlTree() {}

