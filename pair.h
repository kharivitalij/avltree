//
// Created by kharivitalij on 7.5.18.
//

#ifndef AVLTREE_PAIR_H
#define AVLTREE_PAIR_H


#include "node.h"

class Pair {
public:
    Node* min;
    Node* ptr;
    Pair(Node* m, Node* p);
};


#endif //AVLTREE_PAIR_H
