//
// Created by kharivitalij on 28.4.18.
//

#ifndef AVLTREE_NODE_H
#define AVLTREE_NODE_H

#include "iostream"

using namespace std;

class Node {
public:
    int m_key;
    int m_height;
    Node* m_left;
    Node* m_right;
    Node* m_parent;
    void fix_height();
    int lr_diff();
    void show(int tree_height, int current_height);
    string make_str(int padding);
    Node(int key);
};

#endif //AVLTREE_NODE_H