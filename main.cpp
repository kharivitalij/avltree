#include <iostream>
#include "avlTree.h"


int main() {
    avlTree* tree = new avlTree();
    for (int i = 0; i < 10; i++) {
        tree->m_root = tree->add(new Node(i));
    }
    tree->show();
    cout<<"-----------------------------"<<endl;
    for (int i = 0; i < 10; i++) {
        tree->m_root = tree->remove(tree->m_root, new Node(i));
    }
    cout<<"-----------------------------"<<endl;
    tree->show();
    return 0;
}