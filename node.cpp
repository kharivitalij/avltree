//
// Created by kharivitalij on 28.4.18.
//

#include "node.h"
#include "iostream"

using namespace std;

int get_height(Node* node) {
    if (node == nullptr) return -1;
    return node->m_height;
}

void Node::fix_height() {
    int left = get_height(m_left);
    int right = get_height(m_right);
    m_height = left >= right ? left + 1 : right + 1;
}

int Node::lr_diff() {
    return get_height(m_left) - get_height(m_right);
}

void Node::show(int tree_height, int current_height) {
    cout<<make_str(tree_height - current_height)<<endl;
}

string Node::make_str(int padding) {
    string s = "";
    for (int i = 0; i < padding; i++) {
        s += "   ";
    }
    return s + to_string(m_key);
}

Node::Node(int key) {
    m_key = key;
    m_height = 0;
}

